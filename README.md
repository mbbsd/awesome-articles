# Awesome Mathematical Articles #

[![Awesome](https://awesome.re/badge-flat2.svg)](https://awesome.re)

A curated list of awesome mathematical articles.

## Conformal geometry ##

    - Conformal geometry and fully nonlinear equations;

## Submanifold geometry ##

### Simons' type formula ###

    - A formula of Simons' type and hypersurfaces with constant mean curvature;
    - Gap Theorems for minimal submanifolds of a hyperbolic space;
    - Simons' type equation for f-minimal hypersurfaces and applications;
